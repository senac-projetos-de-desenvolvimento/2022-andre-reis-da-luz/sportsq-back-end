import { Router } from "express"
import { usuarioCreate, usuarioDelete, usuarioIndex, usuarioLogin, usuarioUpdate, usuarioValida } from "./controllers/UsuarioController.js"
import {
      viagemCalculaDistancia,
      viagemCreate, viagemDestroy, viagemEstatisticas, viagemIndex, viagemOrdem,
      viagemSearch, viagemSearchPlaca, viagemUpdate
} from "./controllers/ViagemController.js"
import { veiculoCreate, veiculoIndex, veiculoSearch,veiculoMedia, veiculoUpdate, veiculoSearchPlaca, veiculoRetornaPlacas } from "./controllers/VeiculoController.js"

const router = Router()

router.get('/viagens', viagemIndex)
      .post('/viagens', viagemCreate)
      .put('/viagens/:id', viagemUpdate)
      .delete('/viagens/:id', viagemDestroy)
      .get('/viagens/:id', viagemSearch)
      .get('/viagens/pesquisa/:pesquisaPlaca', viagemSearchPlaca)
      .get('/viagens/Ordem/alfabetica', viagemOrdem)
      .post('/viagens/calcular/:id', viagemCalculaDistancia)
      .post('/viagens/estatisticas', viagemEstatisticas)

      .get('/usuarios/listar', usuarioIndex)
      .get('/usuarios/valida/:username', usuarioValida)
      .post('/usuarios/cadastrar', usuarioCreate)
      .post('/usuarios/login', usuarioLogin)
      .post('/usuarios/deletar', usuarioDelete)
      .post('/usuarios/atualizar', usuarioUpdate)

      .get('/veiculos', veiculoIndex)
      .post('/veiculos', veiculoCreate)
      .get('/veiculos/:id', veiculoSearch)
      .get('/veiculos/placa/:placa', veiculoSearchPlaca)
      .get('/veiculos/retornarplacas/:id', veiculoRetornaPlacas)
      .get('/media/:id', veiculoMedia)
      .post('/veiculos/atualizar', veiculoUpdate)

export default router