# Transport Calculator Back-End

Back-end do aplicativo Transport Calculator

## O que é o Transport Calculator

Tranport Calculator é um aplicativo que tem como função auxiliar em orçamentos para empresas ou pessoas que pretendem calcular gastos para meios de transporte rodoviário, que utilizam veículos como caminhões, motos, taxis, carros, ônibus, etc, para fazer entregas ou transportar pessoas


## O que fazemos

Nosso aplicativo calcula gastos e guarda rotas para facilitar na administração da sua empresa, usamos dados como tipo de rua, distância percorrida, tipo de veículo (carro, moto, caminhão, ônibus), média do veículo, pedágio, desgaste de peças e pneus, entre outros para calcular quanto dinheiro será gasto em determinada rota

## Como rodar o back-end do aplicativo:

clone o projeto da maneira que preferir, depois, dentro da pasta do projeto em um terminal digite:

```
npm i
npm run start

```

Pronto, o back-end deve estar sendo criado, apenas aguarde