import { sequelize } from '../databases/conecta.js';
import { Usuario } from '../models/Usuario.js';
import { Veiculo } from '../models/Veiculo.js';
import { Viagem } from '../models/Viagem.js';
import { Op, or, where } from 'sequelize'

export const viagemIndex = async (req, res) => {
  try {
    const viagens = await Viagem.findAll({
      order: [
        ['id', 'DESC']
      ]
    })
    res.status(200).json(viagens)
  } catch (error) {
    res.status(400).send(error)
  }
}

// ----------//------------  2  ----------//------------
export const viagemCreate = async (req, res) => {
  const { cliente, motorista, telefone, placa, carga, enderecoInicial, enderecoFinal, valorCobrado, custo, duracaoHoras, emAndamento, usuario_id, veiculo_id, distanciaKm, dataDaViagem } = req.body

  const veiculoJaCriado = await Veiculo.findOne({ where: { placa: placa } })
  const usuarioJaCriado = await Usuario.findOne({ where: { id: usuario_id } })

  if (!veiculoJaCriado || !usuarioJaCriado) {
    console.log('Veiculo ou usuário não encontrados')
    res.status(400).json({ id: 0, msg: "Erro, usuario ou veiculo nao encontrados " })
  } else {
    if (usuarioJaCriado.get("precoCombustivel")) {

      try {
        const veiculo_id = veiculoJaCriado.get('id')
        const viagem = await Viagem.create({
          cliente, motorista, telefone, placa, carga, enderecoInicial, enderecoFinal, valorCobrado, custo, duracaoHoras, emAndamento, usuario_id, distanciaKm, veiculo_id, dataDaViagem
        });
        res.status(201).json(viagem)
      } catch (error) {
        res.status(400).send(error)
      }
    } else { res.status(400).json({ id: 0, msg: "Favor antes de criar uma viagem atualize o seu cadastro com o preço que você paga no combustível" }) }
  }
}

export const viagemUpdate = async (req, res) => {
  const { id } = req.params
  const { cliente, motorista, telefone, placa, carga, enderecoInicial, enderecoFinal, valorCobrado, usuarioId, emAndamento } = req.body

  const viagemEncontrada = await Viagem.findOne({ where: { id: id } })
  if (!viagemEncontrada) {
    res.status(400).json({ id: 0, msg: "Erro... Viagem não encontrada" })
    return
  }

  if (!cliente) {
    cliente = viagemEncontrada.cliente
  }

  try {
    const viagem = await Viagem.update({
      cliente, motorista, telefone, placa, carga, enderecoInicial, enderecoFinal, valorCobrado, usuarioId, emAndamento
    }, {
      where: { id }
    });
    res.status(200).json(viagem)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const viagemDestroy = async (req, res) => {
  const { id } = req.params
  try {
    const viagem = await Viagem.destroy({
      where: { id }
    });
    res.status(200).json(viagem)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const viagemSearch = async (req, res) => {
  const { id } = req.params
  try {
    const viagem = await Viagem.findByPk(id);
    if (viagem) {
      res.status(200).json(viagem)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Viagem não encontrada." })
    }
  } catch (error) {
    res.status(400).send(error)
  }
}

// ----------//------------  3.1  ----------//------------
export const viagemSearchPlaca = async (req, res) => {
  const { pesquisaPlaca } = req.params;
  try {
    const viagem = await Viagem.findAll({
      where: {
        placa: {
          [Op.like]: `%${pesquisaPlaca}%`
        }
      }
    });
    if (viagem) {
      res.status(200).json(viagem)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Viagem não encontrada." })
    }
  } catch (error) {
    res.status(400).send(error)
  }
}

//ordes desc
export const viagemOrdem = async (req, res) => {
  const viagem = await Viagem.findAll({
    order: [
      ['enderecoFinal', 'ASC']
    ]
  })

  if (viagem) {
    res.status(200).json(viagem)
  } else {
    res.status(200).json({ id: 0, msg: "Erro... Viagem não encontrada." })
  }
}

export const viagemCalculaDistancia = async (req, res) => {
  const { id } = req.params

  try {
    const viagem = await Viagem.findByPk(id);

    if (!viagem) {
      res.status(400).send('Viagem não encontrada')
    }


    const distancia = viagem.get("distanciaKm")
    const placa = viagem.get("placa")

    const veiculo = await Veiculo.findOne({ where: { placa: placa } })
    const media = veiculo.media

    const gasto = (distancia / media).toFixed(2)

    res.status(200).send(`gasto de ${gasto}L`)
  } catch {
    res.status(400)
  }
}

export const viagemEstatisticas = async (req, res) => {
  const { placa, usuario_id, dataInicial, dataFinal } = req.body

  try {
    const viagens = await Viagem.findAll({
      where: { 
        placa: placa, 
        usuario_id: usuario_id, 
        dataDaViagem: { 
        [Op.between]: [dataInicial, dataFinal]} 
      },
    })

    let distanciaTotal = 0
    let rendimentoBruto = 0
    let numeroDeViagens = viagens.length
    let tempoTotal = 0
    for (let viagem in viagens) {
      distanciaTotal += viagens[viagem].distanciaKm
      rendimentoBruto += viagens[viagem].valorCobrado
      tempoTotal += viagens[viagem].duracaoHoras
    }
    let kmMedio = distanciaTotal / numeroDeViagens
    res.status(200).send(`Distância total percorrida: ${distanciaTotal} Km - Rendimento bruto: R$${rendimentoBruto} reais - Total de viagens: ${numeroDeViagens}, Km médio percorrido por viagem: ${kmMedio}, tempo total rodado do veículo (em horas): ${tempoTotal}`)
  } catch {
    res.status(400)
  }
}