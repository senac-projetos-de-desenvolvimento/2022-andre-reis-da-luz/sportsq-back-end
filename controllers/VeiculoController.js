import { sequelize } from '../databases/conecta.js';
import { Usuario } from '../models/Usuario.js';
import { Veiculo } from "../models/Veiculo.js";

export const veiculoIndex = async (req, res) => {
  try {
    const veiculos = await Veiculo.findAll()
    res.status(200).json(veiculos)
  } catch {
    res.status(400).send(error)
  }
}

export const veiculoCreate = async (req, res) => {
  const { placa, marca, tipo, media, usuario_id } = req.body


  const veiculoJaCriado = await Veiculo.findOne({ where: { placa: placa } })
  const usuarioJaCriado = await Usuario.findOne({ where: { id: usuario_id } })

  if (usuarioJaCriado && !veiculoJaCriado) {
    try {
      const veiculo = await Veiculo.create({ placa, marca, tipo, media, usuario_id })
      res.status(201).json(veiculo)
    } catch {
      res.status(400).send("Falha na criação do veículo, verifique se o Id do usuário existe e se a placa do veículo já não está registrada")
    }
  } else {
    res.status(400).send('Placa do veículo já registrada ou usuário não existente')
  }
}

export const veiculoDestroy = async (req, res) => {
  const { id } = req.params
  try {
    const veiculo = await Veiculo.destroy({
      where: { id }
    });
    res.status(200).json(veiculo)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const veiculoSearch = async (req, res) => {
  const { id } = req.params
  try {
    const veiculo = await Veiculo.findOne({ where: { usuario_id: id } })
    if (veiculo) {
      res.status(200).json(veiculo)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Veiculo não encontrado." })
    }
  } catch (error) {
    res.status(400).send('Deu errado')
  }
}

export const veiculoRetornaPlacas = async (req, res) => {
  const { id } = req.params
  try {
    const veiculos = await Veiculo.findAll({ where: { usuario_id: id } })
    if (veiculos) {
      let placas = []
      for(let veiculo in veiculos){
        placas.push(veiculos[veiculo].placa)
      }
      res.status(200).json(placas)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Veiculo não encontrado." })
    }
  } catch (error) {
    res.status(400).send('Deu errado')
  }
}

export const veiculoSearchPlaca = async (req, res) => {
  const { placa } = req.params
  try {
    const veiculo = await Veiculo.findOne({ where: { placa: placa } })
    if (veiculo) {
      res.status(200).json(veiculo)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Veiculo não encontrado." })
    }
  } catch (error) {
    res.status(400).send('Deu errado')
  }
}

export const veiculoMedia = async (req, res) => {
  const { id } = req.params
  try {
    const veiculo = await Veiculo.findOne({ where: { id: id } })
    const veiculoMedia = veiculo.media
    if (veiculo) {
      res.status(200).json(veiculoMedia)
    } else {
      res.status(200).json({ id: 0, msg: "Erro... Veiculo não encontrado." })
    }
  } catch (error) {
    res.status(400).send('Deu errado')
  }
}

export const veiculoUpdate = async (req, res) => {
  const { id, placa, marca, tipo, media } = req.body

  const veiculoJaCriado = await Veiculo.findOne({ where: { id: id } })

  if (veiculoJaCriado) {
    veiculoJaCriado.set({
      id: id,
      placa: placa,
      marca: marca,
      tipo: tipo,
      media: media
    });

    await veiculoJaCriado.save()
    res.status(200).send(`Veiculo ${placa} atualizado`)

  } else {
    res.status(404).send('Veiculo não encontrado')
  }
}

  export const veiculoEstatisticas = async(req, res) => {
    const { placa } = req.body

    
  }