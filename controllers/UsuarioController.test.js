import request from 'supertest'
import app from '../app'

const baseURL = 'localhost:3000/usuarios'

describe("Testes de usuário", () => {
    const novoUsuario = {
        "username": "Light JEST" + Date.now(),
        "password": "123456"
    }
    it("Cria um usuário e depois apaga ele", async () => {

      const responsePost = await request(app).post(`/usuarios/cadastrar`).send(novoUsuario)
      expect(responsePost.body).toMatchObject(novoUsuario);

      const responseDelete = await request(app).post(`/usuarios/deletar`).send({
        "id": responsePost.body.id
      })

      expect(responseDelete.text).toEqual(`Usuário ${novoUsuario.username} deletado`)
    }, 30000);
  });