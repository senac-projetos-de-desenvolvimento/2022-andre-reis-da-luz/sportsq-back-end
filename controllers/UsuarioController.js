import { sequelize } from '../databases/conecta.js';
import { Usuario } from "../models/Usuario.js";

export const usuarioIndex = async (req, res) => {
    try {
        const usuarios = await Usuario.findAll()
        res.status(200).json(usuarios)
    } catch {
        res.status(400).send(error)
    }
}

export const usuarioCreate = async (req, res) => {
    const { password, username } = req.body

    const usuarioJaCriado = await Usuario.findOne({ where: { username: username } })
    if (usuarioJaCriado) {
        res.status(401).send("Nome de usuário já existente")
    }

    try {
        const usuario = await Usuario.create({ password, username})
        res.status(201).json(usuario)
    } catch {
        res.status(400).send(error)
    }
}

export const usuarioLogin = async (req, res) => {
    const { password, username } = req.body

    const usuarioJaCriado = await Usuario.findOne({ where: { username: username } })
    if (usuarioJaCriado.password == password) {
        res.status(200).json(usuarioJaCriado)
    }
    else {
        res.status(400).send('Usuário ou senha estão incorretos.')
    }
}

export const usuarioDelete = async (req, res) => {
    const { id } = req.body

    const usuarioJaCriado = await Usuario.findOne({ where: { id: id } })

    if (usuarioJaCriado) {
        const nome = usuarioJaCriado.get("username")
        usuarioJaCriado.destroy({
            where: { id }
        })
        res.status(200).send(`Usuário ${nome} deletado`)
    } else {
        res.status(404).send('Usuario não encontrado')
    }
}

export const usuarioUpdate = async (req, res) => {
    var { id, username, password, precoCombustivel } = req.body

    const usuarioJaCriado = await Usuario.findOne({ where: { id: id } })

    if(!usuarioJaCriado){
        res.status(404).send('Usuario não encontrado')
    }

    if (!username) {
        username = usuarioJaCriado.get("username")
    }
    if (!password) {
        password = usuarioJaCriado.get("password")
    }

    if (!precoCombustivel) {
        precoCombustivel = usuarioJaCriado.get("precoCombustivel")
    }

    if (usuarioJaCriado) {
        usuarioJaCriado.set({
            username: username,
            password: password,
            precoCombustivel: precoCombustivel
        });

        await usuarioJaCriado.save()
        res.status(200).send(`Usuário ${username} atualizado`)

    } else {
        res.status(404).send('Usuario não encontrado')
    }
}

export const usuarioValida = async (req, res) => {
    const {username} = req.params
    const usuarioJaCriado = await Usuario.findOne({ where: { username: username } })
    console.log(usuarioJaCriado.id)

    if (usuarioJaCriado) {
        res.status(200).send(usuarioJaCriado)

    } else {
        res.status(404).send('Usuario não encontrado')
    }

}