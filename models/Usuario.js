import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Viagem } from './Viagem.js';
import { Veiculo } from './Veiculo.js';

export const Usuario = sequelize.define('Usuario', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  password: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  username: {
    unique: true,
    type: DataTypes.STRING(20),
    allowNull: false,
  },
  precoCombustivel: {
    type: DataTypes.DECIMAL(5,2)
  }
}, {
  tableName: "Usuario"
});

Usuario.hasMany(Viagem, {
  foreignKey: 'usuario_id'
})
Viagem.belongsTo(Usuario, {
  foreignKey: {
    name: 'usuario_id',
    allowNull: false
  }
})

Usuario.hasMany(Veiculo, {
  foreignKey: 'usuario_id'
})
Veiculo.belongsTo(Usuario, {
  foreignKey: {
    name: 'usuario_id',
    allowNull: false
  }
})