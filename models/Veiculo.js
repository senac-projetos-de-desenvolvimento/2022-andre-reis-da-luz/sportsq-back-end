import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';
import { Viagem } from './Viagem.js';

export const Veiculo = sequelize.define('Veiculo', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  placa: {
    unique: true,
    type: DataTypes.STRING(7),
    allowNull: false
  },
  marca: {
    type: DataTypes.STRING(20)
  },
  media: {
    type: DataTypes.DECIMAL(2,2),
    allowNull: false
  },
  tipo: {
    type: DataTypes.STRING(),
    allowNull: false
  }
}, {
  tableName: "Veiculo"
});

Veiculo.hasMany(Viagem, {
  foreignKey: 'veiculo_id'
})
Viagem.belongsTo(Veiculo, {
  foreignKey: {
    name: 'veiculo_id',
    allowNull: false
  }
})