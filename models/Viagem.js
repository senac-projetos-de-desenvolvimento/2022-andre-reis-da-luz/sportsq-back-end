import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/conecta.js';

export const Viagem = sequelize.define('Viagem', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  cliente: {
    type: DataTypes.STRING(80),
    allowNull: false
  },
  motorista: {
    type: DataTypes.STRING(80),
  },
  telefone: {
    type: DataTypes.STRING(15)
  },
  placa: {
    type: DataTypes.STRING(7)
  },
  carga: {
    type: DataTypes.STRING(80)
  },
  enderecoInicial: {
    type: DataTypes.STRING(80),
    allowNull: false
  },
  enderecoFinal: {
    type: DataTypes.STRING(80),
    allowNull: false  
  },
  valorCobrado: {
    type: DataTypes.DECIMAL(12,2)
  },
  custo: {
    type: DataTypes.DECIMAL(12,2)
  },
  duracaoHoras: {
    type: DataTypes.INTEGER(3),
  },
  emAndamento: {
    type: DataTypes.BOOLEAN()
  },
  distanciaKm: {
    type: DataTypes.DECIMAL(5,1)
  },
  dataDaViagem: {
    type: DataTypes.DATEONLY
  },
}, {
  tableName: "Viagens"
});